import java.util.Optional;
import java.util.function.Consumer;

public class Optionals {

    public static void main(String[] args) {

        Optional<Object> emptyOptional = Optional.empty();
        try {
            // throws NoSuchElementException.
            emptyOptional.get();
        } catch (Exception e) {
            System.out.println(e);
        }

        Optional<Object> nullOptional = Optional.ofNullable(null);
        System.out.println(nullOptional.orElse("orElse value"));
        try {
            nullOptional.orElseThrow(IllegalArgumentException::new);
        } catch (Exception e) {
            System.out.println(e);
        }

        Optional<String> mario = Optional.of("Mario");
        System.out.println(mario.isPresent() + ": " + mario.get());

        Consumer<String> printer = s -> System.out.println(s);
        mario.ifPresent(printer);

        // throws NPE.
        Optional.of(null);
    }
}
