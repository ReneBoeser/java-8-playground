import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

class PredicateImpl {

    static void test(String text, Predicate<String> p) {
        if (p.test(text)) {
            System.out.println(text);
        }
    }

    public static void main(String[] args) {

        Predicate<String> p = s -> {
            return s.length() > 0;
        };
        System.out.println(p.test("true"));
        test("Holy moly!", p);
        // NPE:
        // System.out.println(p.test(null));

        IntPredicate ip = i -> i == 7;
        System.out.println(ip.test(3));
        System.out.println(ip.test(7));
        // Compiler error:
        // System.out.println(ip.test("bla"));

        DoublePredicate dp = d -> d == 7.07;
        System.out.println(dp.test(7));
        System.out.println(dp.test(7.07));

    }
}