import java.util.function.*;

class FunctionImpl {
    
    public static void main(String[] args) {
        
        Function<String, Boolean> f = s -> new Boolean(s);
        
        System.out.println(f.apply("true"));
        System.out.println(f.apply("false"));
        System.out.println(f.apply(null));
        
        IntFunction<String> intF = i -> ""+i;
        System.out.println(intF.apply(8));
        
        IntToDoubleFunction itdF = i -> new Double(i);
        System.out.println(itdF.applyAsDouble(100));
        
        ToLongFunction<Double> dToLong = d -> Math.round(d);
        System.out.println(dToLong.applyAsLong(10.91));
        
        ToIntBiFunction<Integer,Integer> ti = (x,y) -> x*y;
        System.out.println(ti.applyAsInt(2,5));
    }
}