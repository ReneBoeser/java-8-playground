import java.util.function.*;

class ConsumerImpl {
    
    public static void main(String[] args) {
        
        Consumer<String> c = s -> System.out.println(s);
        c.accept("whoop whoop");
        
        IntConsumer ic = i -> System.out.println("Your int: "+i);
        ic.accept(123);
        // Compiler error:
        // ic.accept(123.4);
        // ic.accept(null);
        
        DoubleConsumer dc = d -> System.out.println("Double: "+d);
        dc.accept(1.99);
        dc.accept(1);
    }
}