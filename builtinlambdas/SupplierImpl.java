import java.util.function.*;

class SupplierImpl {
    
    public static void main(String[] args) {
        
        Supplier<Todo> s = () -> new Todo("buy milk");
        Supplier<Todo> s2= Todo::new;
        System.out.println(s.get());
        System.out.println(s2.get());
        
        DoubleSupplier iS = () -> Math.random();
        System.out.println(iS.getAsDouble());
    }
}