import java.util.function.*;

class BinaryOperatorImpl {
    
    public static void main(String[] args) {
        
        BinaryOperator<Double> bO = (x,y) -> x-y;
        System.out.println(bO.apply(1.0,2.5));
        // Compiler error:
        // System.out.println(bO.apply(1.0,2));
        
        IntBinaryOperator iB = (x,y) -> x+y;
        System.out.println(iB.applyAsInt(1,2));
    }
}