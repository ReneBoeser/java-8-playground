import java.util.function.*;

class UnaryOperatorImpl {
    
    public static void main(String[] args) {
        
        UnaryOperator<String> u = s -> "Hello " + s;
        System.out.println(u.apply("Sir"));
        
        IntUnaryOperator iU = i -> i+1;
        System.out.println(iU.applyAsInt(1));
    }
}