import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class UseWatchService {

    public static final Path PATH = Paths.get(System.getProperty("user.home"));

    public static void main(String[] args) throws IOException {

        Instant start = Instant.now();

        try (WatchService watchService = FileSystems.getDefault().newWatchService()) {

            WatchKey watchKey = PATH.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY,
                    StandardWatchEventKinds.OVERFLOW);

            System.out.println("Entering take() snippet");
            WatchKey taken;
            if ((taken = watchService.take()) != null) {
                taken.pollEvents().forEach(printWatchEvents());
                // if not reset, the WatchKey would not be put back into queue!!!
                taken.reset();
            }

            System.out.println("Entering poll() snippet");
            WatchKey polledWatchKey = watchService.poll(10, TimeUnit.SECONDS);

            polledWatchKey.pollEvents().forEach(printWatchEvents());
            polledWatchKey.reset();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Instant end = Instant.now();
        System.out.println("Closing app after " + Duration.between(end, start));
    }

    private static Consumer<WatchEvent<?>> printWatchEvents() {

        return watchEvent -> System.out.println(
                "File: " + watchEvent.context() + "\n"
                        + "Amount: " + watchEvent.count() + "\n"
                        + "Kind: " + watchEvent.kind() + "\n");
    }
}
