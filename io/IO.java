import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class IO {

    public static final String PATH_STRING = "C:\\Users\\rebo\\AndroidStudioProjects\\java-8-playground";
    public static final Path PATH = Paths.get(PATH_STRING);

    public static void main(String[] args) throws IOException {

        BiPredicate<Path, BasicFileAttributes> onlyJavaFiles =
                (path, basicFileAttributes) -> String.valueOf(path).endsWith(".java");

        try (Stream<Path> pathStream =
                     Files.find(Paths.get(PATH_STRING), 5, onlyJavaFiles)) {

            String foundFiles = pathStream
                    .map(Path::toString)
                    .collect(Collectors.joining("\n"));
            System.out.println(foundFiles);
        }

        try (Stream<Path> pathStream = Files.find(Paths.get(PATH_STRING), 5, onlyJavaFiles)) {

            // .lines returns a Stream!
            try (Stream<String> lines = Files.lines(pathStream.findFirst().get())) {
                lines.forEach(System.out::println);
            }
        }

        try (Stream<Path> pathStream = Files.find(Paths.get(PATH_STRING), 5, onlyJavaFiles)) {

            // .readAllLines returns a List!
            List<String> lines = Files.readAllLines(pathStream.findFirst().get());
            lines.forEach(System.out::println);
        }

        try (Stream<Path> pathStream = Files.walk(PATH, 0)) {

            pathStream.forEach(System.out::println);
        }
    }
}