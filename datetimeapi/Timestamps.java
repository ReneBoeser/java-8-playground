import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

public class Timestamps {

    public static final String NEW_LINE = "\n\n";
    static Instant instant;

    public static void main(String[] args) {

        final StringBuilder stringBuilder = new StringBuilder();

        instant = Instant.now();
        stringBuilder.append(instant.toString());

        instant = Instant.parse("1969-12-31T23:59:00Z");
        stringBuilder.append(NEW_LINE + instant.getEpochSecond());

        instant = Instant.now(Clock.system(ZoneId.of("Europe/Paris")));
        stringBuilder.append(NEW_LINE + instant);

        System.out.println(stringBuilder.toString());
    }
}
