import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class DatesAndTimes {

    public static final String NEW_LINE = "\n\n";
    static LocalDate localDate;
    static LocalTime localTime;
    static LocalDateTime localDateTime;

    public static void main(String[] args) {

        StringBuilder stringBuilder = new StringBuilder();

        localDate = LocalDate.now();
        stringBuilder.append(localDate);

        localDate = LocalDate.of(2019, Month.OCTOBER, 17);
        stringBuilder.append(NEW_LINE + localDate);

        localDate = LocalDate.ofYearDay(2019, 123);
        stringBuilder.append(NEW_LINE + localDate);

        localDate = LocalDate.parse("2019-12-31");
        stringBuilder.append(NEW_LINE + localDate);

        localTime = LocalTime.of(20, 15);
        stringBuilder.append(NEW_LINE + localTime);

        localTime = LocalTime.ofSecondOfDay(12345);
        stringBuilder.append(NEW_LINE + localTime);

        localDateTime = LocalDateTime.of(localDate, localTime);
        stringBuilder.append(NEW_LINE + localDateTime);

        localDateTime = LocalDateTime.of(2020, Month.FEBRUARY, 20, 20, 20, 20);
        stringBuilder.append(NEW_LINE + localDateTime);

        System.out.println(stringBuilder.toString());
    }
}
