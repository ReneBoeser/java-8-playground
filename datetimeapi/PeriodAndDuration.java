import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

class PeriodAndDuration {

    public static final String NEW_LINE = "\n\n";
    static Period period;

    public static void main(String[] args) {

        StringBuilder stringBuilder = new StringBuilder();

        LocalDate now = LocalDate.now();
        LocalDate birthday = LocalDate.of(1987, 11, 28);

        // Yes, I'm minus.
        period = Period.between(now, birthday);
        long ageInDays = ChronoUnit.DAYS.between(now, birthday);
        System.out.printf("You are %d years, %d months and %d days old - or %d days.",
                period.getYears(), period.getMonths(), period.getDays(), ageInDays);


        Instant before = Instant.now();
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Instant after = Instant.now();
        Duration between = Duration.between(before, after);
        stringBuilder.append(NEW_LINE + "Duration in ns: " + between.toMillis() +
                NEW_LINE + " in ms: " + between.toNanos());

        System.out.println(stringBuilder.toString());
    }
}