package concurrency;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class UseCyclicBarrier {

    public static void main(String[] args) {

        Usage usage = new Usage();
        usage.start();
    }

    static class Usage {

        private CyclicBarrier cyclicBarrier;

        public void start() {
            Runnable barrierAction = () -> System.out.println("Barrier point reached by thread " + Thread.currentThread().getName());
            int nrOfThreads = 3;
            cyclicBarrier = new CyclicBarrier(nrOfThreads, barrierAction);

            for (int i = 0; i < nrOfThreads; i++) {

                Thread thread = new Thread(new Worker());
                thread.start();
            }
        }

        class Worker implements Runnable {

            @Override
            public void run() {
                System.out.println("Thread " + Thread.currentThread().getName() + " waiting at barrier point");
                try {
                    // await() is synchronous and the calling thread suspends until all threads have called that method.
                    int awaitIdx = cyclicBarrier.await();
                    System.out.println("Thread ID: " + awaitIdx + ", name: " + Thread.currentThread().getName() + " continues.");
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
