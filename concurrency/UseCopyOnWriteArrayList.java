package concurrency;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class UseCopyOnWriteArrayList {

    public static void main(String[] args) {

        CopyOnWriteArrayList<Integer> integers = new CopyOnWriteArrayList<>(Arrays.asList(1, 2, 3));
        Iterator<Integer> iterator = integers.iterator();

        integers.add(4);
        iterator.forEachRemaining(System.out::println);

        integers.iterator().forEachRemaining(System.out::println);

        try {
            iterator.remove();
        } catch (UnsupportedOperationException e) {
            System.out.println("Yes, remove() on an iterator from a CopyOnWriteArrayList throws a:");
            e.printStackTrace();
        }
    }
}
