@FunctionalInterface
public interface FunctionalIface {

    default void defaultMethod() {
        System.out.println("default BOOM!");
        singleAbstractMethod();
        staticSwag();
    }

    default void anotherDefaultMethod() {
        System.out.println("another default!");
    }

    void singleAbstractMethod();

    //void anotherAbstract(); // not possible with @FunctionalInterface

    static void staticSwag() {
        System.out.println("Staaaatic.");
        //defaultMethod(); // not possible
        //singleAbstractMethod(); // not possible
    }

    // Does not count bcuz it's Object.toString.
    @Override
    String toString();
}
