class Impl implements NotAFunctionalIface {

    @Override
    public void singleAbstractMethod() {
        System.out.println("ARRRRR!");
    }

    public static void main(String[] args) {

        Impl impl = new Impl();
        impl.singleAbstractMethod();
        // FunctionalIface.defaultMethod(); // not possible
        impl.defaultMethod();

        // impl.staticSwag(); // not possible
        // the ONLY way to call that static method!
        FunctionalIface.staticSwag();

        System.out.println(impl.toString());

        impl.anotherAbstract();
    }

    @Override
    public void defaultMethod() {

        NotAFunctionalIface.super.anotherDefaultMethod();
        System.out.println("Was initially implemented but got overridden as abstract...");
    }

    @Override
    public void anotherDefaultMethod() {

        System.out.println("This should not get printed...");
    }

    @Override
    public void anotherAbstract() {

        System.out.println("another abstract method!");
    }
}
