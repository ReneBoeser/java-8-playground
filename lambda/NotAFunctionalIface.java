public interface NotAFunctionalIface extends FunctionalIface {

    @Override
    void defaultMethod();

    @Override
    default void anotherDefaultMethod() {

        System.out.println("anotherDefault got overridden");
    }

    void anotherAbstract();

    // default int hashCode() {} // does NOT compile.
}
