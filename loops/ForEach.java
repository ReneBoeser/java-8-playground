import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

class ForEach {
    public static void main(String[] args) {

        List<String> strings = Arrays.asList("Luke", "Jedi");
        strings.forEach(System.out::println);

        BiConsumer<Integer, Integer> addAndPrint = (a, b) -> System.out.println(a + b);
        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 1);
        map.put(2, 2);
        map.forEach(addAndPrint);
    }
}