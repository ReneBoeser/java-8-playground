package tests;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Test1 {

    public static void main(String[] args) throws IOException {

        //#1
        Stream.of("One", "Four", "Fivee")
                .mapToInt(s -> s.length())
                .filter(len -> len > 3)
                .peek(System.out::println)
                .limit(2);

        //#2
        int ch = 0;
        String file = "C:\\Users\\rebo\\AndroidStudioProjects\\java-8-playground\\README.md";
        try (FileReader reader = new FileReader(file)) {
            while ((ch = reader.read()) != -1) {
                System.out.print((char) ch);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //#3
        CyclicBarrier barrier = new CyclicBarrier(3, () -> System.out.println("Let's play!"));
        Runnable r = () -> {
            System.out.println("\nAwaiting");
            try {
                barrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        };
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        Thread t3 = new Thread(r);
        t1.start();
        t2.start();
        t3.start();

        //#22
        int reduced = Stream.of("one", "two", "three").mapToInt(String::length).reduce(0, (i, i1) -> i + i1);
        System.out.println("Reduced: " + reduced);

        //#26
        IntFunction<UnaryOperator<Integer>> func = i -> j -> i * j;
        System.out.println(func.apply(10).apply(20));

        //# 28
        LocalDate localDate = LocalDate.of(2019, Month.DECEMBER, 20);
        MonthDay todayMonthDay = MonthDay.now();
        MonthDay birth = MonthDay.of(localDate.getMonth(), localDate.getDayOfMonth());
        System.out.println(todayMonthDay.equals(birth) ? "Happy birthday!" : "No, it's not your birthday.");


        // #29
        Base<Number> bN = new Base<Number>();
        Derived<Integer> dI = new Derived<Integer>();
//        Derived<Number> bDN = new Derived<Integer>();


        // #
        List<Integer> integers = Arrays.asList(1, 2, 3);
        integers.replaceAll(i -> i * i);
        System.out.println(integers);

        // #37
        Duration years = ChronoUnit.YEARS.getDuration().multipliedBy(10);
        Duration decadesDuration = ChronoUnit.DECADES.getDuration();
        assert years.equals(decadesDuration) : "years != decades";

        // #39
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\rebo\\AndroidStudioProjects\\java-8-playground\\test\\Test1.java"))) {
            int ch1;
            bufferedReader.skip(6);

            while ((ch1 = bufferedReader.read()) != -1) {
//                System.out.print((char) ch1);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // #42
        Locale locale = Locale.GERMAN;

        // #43
        String[] exams = {"OCAJP 8", "OCPJP 8", "Upgrade to OCPJP 8"};
        Predicate<String> isOCPExam = exam -> exam.contains("OCP");
        List<String> ocpExams = Arrays.stream(exams)
                .filter(exam -> exam.contains("OCP"))
                .collect(Collectors.toList());      // LINE-2
        boolean result =
                ocpExams.stream().anyMatch(exam -> exam.contains("OCA")); // LINE-3
        System.out.println(result);

        // #49
        Path currPath = Paths.get(".");
        try (DirectoryStream<Path> javaFiles = Files.newDirectoryStream(currPath, "*.{java}")) {
            for (Path javaFile : javaFiles) {
                System.out.println(javaFile);
            }
        } catch (IOException ioe) {
            System.err.println("IO Error occurred");
            System.exit(-1);
        }

        // #50
        Path here = Paths.get("D:\\not\\existing.txt");
        Iterator<Path> iterator = here.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }

        // #75
        List<Integer> integers1 = Arrays.asList(1, 2, 3, 4, 5);
        //integers1.removeIf(integer -> integer % 2 == 0);
        System.out.println(integers1);

        // #
    }
}