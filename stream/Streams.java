import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Streams {

    public static void main(String[] args) {

        List<String> alphabet = Arrays.asList("a", "a", "b", "c", "d", "e");

        List<String> processedAlphabet = alphabet.stream()
                .filter(s -> !s.startsWith("b"))
                .distinct()
                .peek(s -> System.out.println("Peek: " + s))
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        processedAlphabet.forEach(System.out::println);

        Optional<String> min = alphabet.stream()
                .min(Comparator.comparing(String::hashCode));
        System.out.println("Min: " + min + ", get: " + min.get());

        Optional<String> max = alphabet.stream().max(Comparator.naturalOrder());
        max.ifPresent(System.out::println);

        Optional<String> any = alphabet.stream().filter(s -> s.contains("a")).findAny();
        System.out.println(any);
        Optional<String> first = alphabet.stream().filter(s -> s.contains("a")).findFirst();
        System.out.println(first);
        long count = alphabet.stream().filter(s -> s.contains("a")).count();
        System.out.println(count);

        List<String> sorted = alphabet.stream()
                .sorted(Comparator.comparing(s -> s.hashCode()))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
        System.out.println(sorted);
    }
}
