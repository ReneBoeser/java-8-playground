import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class FlatMapAndMerge {

    public static void main(String[] args) {

        List<String> prenames = Arrays.asList("Paul", "Michael");
        List<String> surnames = Arrays.asList("Herzog", "Mueller");
        List<List<String>> names = Arrays.asList(prenames, surnames);

        names.stream().map(Object::toString).collect(Collectors.toList()).forEach(System.out::println);

        names.stream().flatMap(Collection::stream).forEach(System.out::println);


        final Map map = new HashMap<Integer, Integer>();
        map.put(1, 1);
        map.put(2, 2);
        map.put(3, null);
        map.put(4, 4);
        BiFunction<Integer, Integer, Integer> replaceIfGreater = (o, o2) -> o2 > o ? o2 : o;
        BiFunction<Integer, Integer, Integer> alwaysNull = (integer, integer2) -> null;

        map.merge(1, 10, replaceIfGreater);
        map.merge(2, 1, replaceIfGreater);
        map.merge(3, 33, replaceIfGreater);
        map.merge(4, 4, alwaysNull);
        System.out.println(map.toString());
    }
}
