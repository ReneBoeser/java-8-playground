import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class LazyStreams {

    public static void main(String[] args) {

        List<String> numbers = Arrays.asList("one", "two", "three", "four");

        Stream<String> stream = numbers.stream()
                .filter(s -> {
                    print(s);
                    return s.length() > 3;
                })
                .map(s -> {
                            print("mapping " + s);
                            return s.toUpperCase();
                        }
                );

        print("Stream was filtered and mapped. Calling terminal method now: ");
        stream.findFirst().ifPresent(System.out::println);
    }

    public static void print(String s) {
        System.out.println(s);
    }
}
