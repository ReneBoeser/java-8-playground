import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class StreamWithLambdas {

    public static void main(String[] args) {

        List<Integer> integers = Arrays.asList(1, 10, 100, 1000);

        Predicate<Integer> greaterOne = integer -> integer > 1;
        Predicate<Integer> smallerThousand = integer -> integer < 1000;
        Consumer<Integer> printLine = System.out::println;

        integers.stream().filter(greaterOne).filter(smallerThousand).forEach(printLine);
    }
}
