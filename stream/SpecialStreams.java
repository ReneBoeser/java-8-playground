import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class SpecialStreams {

    public static void main(String[] args) {

        String bert = "Bert";
        List<String> names = Arrays.asList("Adam", bert, "Cesar");

        if (names.stream().anyMatch(s -> s.equalsIgnoreCase(bert)))
            System.out.println("Found " + bert);

        IntStream intStream = names.stream().mapToInt(s -> s.length());
        System.out.println(intStream.average().getAsDouble());

        IntStream numbers = IntStream.of(1, 2, 3, 4, 5);
        OptionalInt max = numbers.max();
        System.out.println(max.getAsInt());

        IntStream.range(1, 4).max().ifPresent(System.out::println);
        IntStream.rangeClosed(1, 4).max().ifPresent(System.out::println);
        LongStream.range(0, 3).forEach(System.out::println);
        LongStream.rangeClosed(0, 3).forEach(System.out::println);
    }
}
